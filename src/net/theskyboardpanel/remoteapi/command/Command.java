/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.command;

import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;

/**
 * 
 * This interface is used to create a command
 * 
 * @author Roe
 *
 */
public interface Command
{

	/**
	 * 
	 * This method is used to get the name of the command
	 * 
	 * @return name the name of the command
	 */
	public String getName();

	/**
	 * 
	 * This method is used to set the name of the command
	 * 
	 * @param name the name of the command
	 */
	public void setName(String name);
	
	/**
	 * 
	 * This method is used to get the string the user have to enter to trigger the command
	 * 
	 * @return command the command string
	 */
	public String getCommand();
	
	/**
	 * 
	 * This method is used to set the string the user have to enter to trigger the command
	 * 
	 * @param command the command string
	 */
	public void setCommand(String command);
	
	/**
	 * 
	 * This method is used to get the description about the command
	 * 
	 * @return description the command description
	 */
	public String getDescription();
	
	/**
	 * 
	 * This method is used to set the description about the command
	 * 
	 * @param description the command description
	 */
	public void setDescription(String description);
	
	/**
	 * 
	 * This method is used to get the alternatives to the original command string
	 * 
	 * @return alternatives the command string alternatives
	 */
	public String[] getAlternatives();
	
	/**
	 * 
	 * This method is used to set the alternatives to the original command string
	 * 
	 * @param alternatives the command string alternatives
	 */
	public void setAlternatives(String[] alternatives);
	
	/**
	 * 
	 * This method is used to execute this command to a specefic commandsender
	 * 
	 * @param sender the sender to execute the command as
	 * @param args the command arguments
	 * @return the command result
	 * @throws InsufficientPermissionsException this exception is thrown when the command sender have insufficient permissions
	 */
	public boolean executeCommand(CommandSender sender, String[] args) throws InsufficientPermissionsException;
	
	/**
	 * 
	 * This method is used to get the command executor of the command
	 * 
	 * @return the commandexecutor
	 */
	public CommandExecutor getCommandExecutor();
	
	/**
	 * 
	 * This method is used to set the command executor of the command
	 * 
	 * @param executor the command executor to set
	 */
	public void setCommandExecutor(CommandExecutor executor);
	
}