/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.command;

import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;

/**
 * 
 * This interface is used to make a class able to handle command execution
 * 
 * @author Roe
 *
 */
public interface CommandExecutor
{

	/**
	 * 
	 * This method is used to execute the command
	 * 
	 * @param sender the command sender of the command
	 * @param command the command
	 * @param args the arguments for the command
	 * @return result the result of the command. True for success, false for error
	 * @throws InsufficientPermissionsException this exception is thrown when the commandsender have inssufficient permissions
	 */
	public boolean onCommand(CommandSender sender, Command command, String[] args) throws InsufficientPermissionsException;
	
}