package net.theskyboardpanel.remoteapi.command;

public class CommandException extends RuntimeException
{

	private static final long serialVersionUID = -635308419450265378L;

	public CommandException(String message)
	{
		
		super(message);
		
	}
	
}