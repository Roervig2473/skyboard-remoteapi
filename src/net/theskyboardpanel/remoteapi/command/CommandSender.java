/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.command;

import net.theskyboardpanel.remoteapi.permission.Permissionable;

/**
 * 
 * This interface is used to identify a command executor
 * 
 * @author Roe
 *
 */
public interface CommandSender extends Permissionable
{

	/**
	 * 
	 * This method is used to get the username of the Command Sender
	 * 
	 * @return username the username of the command sender
	 */
	public String getUsername();
	
	/**
	 * 
	 * This method is used to set the username of the Command Sender
	 * 
	 * @param username the username of the command sender
	 */
	public void setUsername(String username);
	
	/**
	 * 
	 * This method is used to send a message to the command sender
	 * 
	 * @param message the message to send to the user
	 */
	public void sendMessage(String message);
	
	/**
	 * 
	 * This method is used to execute a command as the command sender
	 * 
	 * @param command the command you want the sender to execute
	 * @param args the arguments you want to pass
	 * @return the command result
	 */
	public boolean executeCommand(String command, String[] args);
	
}