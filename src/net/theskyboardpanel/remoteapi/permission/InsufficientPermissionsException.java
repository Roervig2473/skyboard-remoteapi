package net.theskyboardpanel.remoteapi.permission;

import net.theskyboardpanel.remoteapi.command.CommandSender;

public class InsufficientPermissionsException extends PermissionException
{

	private static final long serialVersionUID = 7252652673146955846L;

	public InsufficientPermissionsException(CommandSender sender)
	{
	
		super("I'm sorry but you have inssuficient permissions to perform this action");

	}

}