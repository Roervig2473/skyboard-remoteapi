package net.theskyboardpanel.remoteapi.permission;

/**
 * 
 * This class is used to throw a permission error
 * 
 * @author Roe
 *
 */
public class PermissionException extends RuntimeException
{

	private static final long serialVersionUID = -3407689356147902054L;

	public PermissionException(String message)
	{
		
		super(message);
		
	}
	
}