/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.permission;

import java.util.ArrayList;

/**
 * 
 * This interface is used to make a class/interface/user/ANYTHING able to have permissions attached
 * 
 * @author Roe
 *
 */
public interface Permissionable
{

	/**
	 * 
	 * This method is used to check if the superclass has the permission
	 * 
	 * @param permission the permission to check
	 * @return true if the superclass has the permission, false if not.
	 */
	public boolean hasPermission(String permission);
	
	/**
	 * 
	 * This method is used to check if the superclass has the permission
	 * 
	 * @param permission the permission to check
	 * @return true if the superclass has the permission, false if not.
	 */
	public boolean hasPermission(Permission permission);
	
	/**
	 * 
	 * This method is used to get all the permissions available for the superclass
	 * 
	 * @return the array of permissions
	 */
	public ArrayList<Permission> getPermissions();
	
	/**
	 * 
	 * This method is used to add a permission to the superclass
	 * 
	 * @param permission the permission to add to the superclass
	 * @throws IllegalArgumentException CAN be thrown when the superclass already have the permission
	 */
	public void addPermission(Permission permission);
	
	/**
	 * 
	 * This method is used to add a permission ot the superclass
	 * 
	 * @param permission the permission to add to the superclass
	 * @throws IllegalArgumentException CAN be thrown when the superclass already have the permission
	 */
	public void addPermission(String permission);
	
	/**
	 * 
	 * This method is used to remove a permission from a superclass
	 * 
	 * @param permission the permission to remove from the superclass
	 * @throws IllegalArgumentException CAN be thrown when the superclass doesn't have the permission
	 */
	public void removePermission(Permission permission);
	
	/**
	 * 
	 * This method is used to remove a permission from a superclass
	 * 
	 * @param permission the permission to remove from the superclass
	 * @throws IllegalArgumentException CAN be thrown when the superclass doesn't have the permission
	 */
	public void removePermission(String permission);
	
}