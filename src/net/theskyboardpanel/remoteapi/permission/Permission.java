/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.permission;

/**
 * 
 * This interface is used to create permissions
 * 
 * @author Roe
 *
 */
public interface Permission
{

	/**
	 * 
	 * This method is used to get the name of the permission
	 * 
	 * @return the name of the permission
	 */
	public String getName();
	
	/**
	 * 
	 * This method is used to set the name of the permission
	 * 
	 * @param name the name of the permission
	 */
	public void setName(String name);
	
	/**
	 * 
	 * This method is used to get the permission string
	 * 
	 * @return the permission string
	 */
	public String getPermission();
	
	/**
	 * 
	 * This method is used to set the permission string
	 * 
	 * @param permission the permission string
	 */
	public void setPermission(String permission);
	
	/**
	 * 
	 * This method is used to get the description about the permission
	 * 
	 * @return the permission description IF set. Else, null
	 * @throws NullPointerException when a permission description isn't available!
	 */
	public String getDescription();
	
	/**
	 * 
	 * This method is used to set the description of the permission
	 * 
	 * @param description the permission description
	 */
	public void setDescription(String description);
	
}