package net.theskyboardpanel.remoteapi.packet;

import java.io.Serializable;

/**
 * 
 * This class is used to send packets between the server and the client
 * 
 * @author Roe
 *
 */
public abstract class Packet implements Serializable
{

	private static final long serialVersionUID = 1374472414847809845L;

	private String name;
	
	/**
	 * 
	 * @param name the packet name
	 */
	public Packet(String name)
	{
	
		this.setName(name);
		
	}
	
	public String getName()
	{
		
		return this.name;
		
	}
	
	public void setName(String name)
	{
		
		this.name = name;
		
	}
	
	/**
	 * 
	 * This method is used to create a new packet from a string
	 * 
	 * @param packetString the string to create the packet from
	 * @return the new packet
	 * @throws PacketException
	 */
	public static Packet newPacketFromString(String packetString) throws PacketException
	{
	
		return null;
		
	}
	
}