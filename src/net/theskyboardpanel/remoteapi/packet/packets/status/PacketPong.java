package net.theskyboardpanel.remoteapi.packet.packets.status;

import net.theskyboardpanel.remoteapi.packet.Packet;

/**
 * @author Roe
 *
 */
public class PacketPong extends Packet
{

	private static final long serialVersionUID = 2632743789967308258L;

	public PacketPong()
	{
	
		super("Pong");

	}

}