/**
 * 
 */
package net.theskyboardpanel.remoteapi.packet.packets.authentication;

import java.io.Serializable;

/**
 *
 * This enum is used to give the result of a authentication
 *
 * @author Roe
 *
 */
public enum AuthenticationResults implements Serializable
{

	SUCCESS,
	SUSPENDED,
	UNKNOWN;
	
}