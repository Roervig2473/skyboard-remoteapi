package net.theskyboardpanel.remoteapi.packet.packets.authentication;

import net.theskyboardpanel.remoteapi.packet.Packet;

/**
 * 
 * This packet is sent after a user have tried to authenticate.
 * 
 * @author Roe
 *
 */
public class PacketAuthenticationResult extends Packet
{

	private static final long serialVersionUID = 2453546234668056378L;
	private final AuthenticationResults result;
	private final PacketAuthenticate authenticationPacket;

	/**
	 * 
	 * @param result the result of the authentication
	 * @param authenticationPacket the original authentication packet sent
	 */
	public PacketAuthenticationResult(AuthenticationResults result, PacketAuthenticate authenticationPacket)
	{
	
		super("PacketAuthenticationResult:" + authenticationPacket.getName() + ":" + result.toString());
		
		this.result = result;
		this.authenticationPacket = authenticationPacket;
		
	}

	/**
	 * 
	 * This method is used to get the result of the authentication
	 * 
	 * @return the result
	 */
	public AuthenticationResults getResult()
	{
	
		return this.result;
	
	}

	/**
	 * 
	 * This method is used to get the original authentication packet
	 * 
	 * @return the packet
	 */
	public PacketAuthenticate getAuthenticationPacket()
	{
	
		return this.authenticationPacket;
	
	}

}