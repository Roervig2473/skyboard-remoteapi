package net.theskyboardpanel.remoteapi.packet.packets.authentication;

import net.theskyboardpanel.remoteapi.packet.Packet;

/**
 * 
 * This packet is used to authenticate a user/system for use with the SKYBoard Remote software
 * 
 * @author Roe
 *
 */
public class PacketAuthenticate extends Packet
{
	
	private static final long serialVersionUID = -7331261734258041227L;
	private final String email;
	private final String password;

	/**
	 * 
	 * @param email the email of the user
	 * @param password the password of the user
	 */
	public PacketAuthenticate(String email, String password)
	{
		
		super("PacketAuthenticate:" + email);
		
		this.email = email;
		this.password = password;
	
	}

	/**
	 * 
	 * This method is used to get the email of the attempted authentication
	 * 
	 * @return the email
	 */
	public String getEmail()
	{
	
		return this.email;
	
	}

	/**
	 * 
	 * This method is used to get the password of the attempted authentication
	 * 
	 * @return the password
	 */
	public String getPassword()
	{
	
		return this.password;
	
	}

}