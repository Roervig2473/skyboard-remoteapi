package net.theskyboardpanel.remoteapi.packet.packets.common;

import net.theskyboardpanel.remoteapi.packet.Packet;

/**
 * 
 * This packet is sent when either the server or the client wants to close the connection.
 * 
 * @author Roe
 *
 */
public class PacketCloseConnection extends Packet
{

	private static final long serialVersionUID = -8184020087565801024L;

	public PacketCloseConnection()
	{
	
		super("PacketCloseConnection");

	}

}