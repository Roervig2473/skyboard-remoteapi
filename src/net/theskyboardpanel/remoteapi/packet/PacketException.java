package net.theskyboardpanel.remoteapi.packet;

/**
 * 
 * This exception is used for when an error occours with a packet
 * 
 * @author Roe
 *
 */
public class PacketException extends Exception
{

	private static final long serialVersionUID = -8100719754007866961L;
	private Packet pocket;
	
	/**
	 * 
	 * @param packet the troubled packet
	 */
	public PacketException(Packet packet)
	{
		
		super("An internal packet error occoured");
		
		this.setPocket(packet);
		
	}

	/**
	 * 
	 * This method is used to get the packet there caused the exception
	 * 
	 * @return the troubled packet
	 */
	public Packet getPocket()
	{
	
		return this.pocket;
	
	}

	private void setPocket(Packet pocket)
	{
	
		this.pocket = pocket;
	
	}
	
}