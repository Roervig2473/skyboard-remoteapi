package net.theskyboardpanel.remoteapi.event;

/**
 * 
 * This enum is used to get the priority of an event
 * 
 * @author Roe
 *
 */
public enum EventPriority
{

	LOWEST,
	LOW,
	REGULAR,
	HIGH,
	HIGHEST,
	RESULT;
	
}