package net.theskyboardpanel.remoteapi.event;

import java.util.ArrayList;

public abstract class Event
{

	private final ArrayList<Listener> handlers = new ArrayList<Listener>();

	public ArrayList<Listener> getHandlers()
	{
	
		return this.handlers;
	
	}
	
}