package net.theskyboardpanel.remoteapi.event;

/**
 * 
 * This interface is used to tell wether an event is cancelable or not
 * 
 * @author Roe
 *
 */
public interface Cancelable
{

	/**
	 * 
	 * This method is used to check if the even was cancelled or not
	 * 
	 * @return cancelled
	 */
	public boolean isCancelled();
	
	/**
	 * 
	 * This method is used to cancel/uncancel an event
	 * 
	 * @param cancelled wether or not the event should be cancelled
	 */
	public void setCancelled(boolean cancelled);
	
}