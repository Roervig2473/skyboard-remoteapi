package net.theskyboardpanel.remoteapi.event;

import java.lang.annotation.*;

/**
 * 
 * This annotation is used to define a method is an Event listener
 * 
 * @author Roe
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler
{
	
	/**
	 * 
	 * This method is used to get the priority of the listener
	 * 
	 * @return the event priority
	 */
	public EventPriority priority() default EventPriority.REGULAR;
	
	/**
	 * 
	 * This method is used to ignore cancelled events
	 * 
	 * @return wether or not to ignore cancelled events
	 */
	public boolean ignoreCancelled() default false;

}