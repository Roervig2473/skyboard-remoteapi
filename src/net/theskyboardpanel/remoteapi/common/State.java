package net.theskyboardpanel.remoteapi.common;

public enum State
{

	DEVELOPMENT,
	BETA,
	RECOMMENDED,
	UNKNOWN;
	
	/**
	 * 
	 * This method is used to get the state enum from a string
	 * 
	 * @param string the state string
	 * @return the state the string equals, returns State.UNKOWN if the string doesn't match
	 */
	public static State getState(String string)
	{
		
		for(State state : values())
		{
			
			if(state.toString().equals(string.toUpperCase()))
			{
				
				return state;
				
			}
			
		}
		
		return UNKNOWN;
		
	}
	
}