package net.theskyboardpanel.remoteapi.common;

import java.net.URL;

/**
 * 
 * This interface is used to define an author
 * 
 * @author Roe
 *
 */
public interface Author
{

	/**
	 * 
	 * This method is used to get the name of the author
	 * 
	 * @return name the name of the author
	 */
	public String getName();
	
	/**
	 * 
	 * This method is used to get a bio for the author
	 * 
	 * @return about the bio of the author
	 */
	public String getAbout();
	
	/**
	 * 
	 * This method is used to get the authors website URL 
	 * 
	 * @return url the url of the website
	 */
	public URL getWebsite();
	
}