package net.theskyboardpanel.remoteapi.common;

/**
 * 
 * This interface is used easy version management<br />
 * Formatting: MAJOR.MINOR.PATCH-STATE
 * 
 * @author Roe
 *
 */
public interface Version
{

	/**
	 * 
	 * This method is used to get the major version of the plugin
	 * 
	 * @return the major version of the plugin
	 */
	public int getMajor();
	
	/**
	 * 
	 * This method is used to get the minor version of the plugin
	 * 
	 * @return the minor version of the plugin
	 */
	public int getMinor();
	/**
	 * 
	 * This method is used to get the patch version of the plugin
	 * 
	 * @return the patch version of the plugin
	 */
	public int getPatch();
	
	/**
	 * 
	 * This method is used to get the plugin state
	 * 
	 * @return the plugin state
	 */
	public State getState();
	
	/**
	 * 
	 * This method is used to get the combined version
	 * 
	 * @return the combined plugin version
	 */
	public String toString();
	
}