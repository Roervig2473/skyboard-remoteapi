package net.theskyboardpanel.remoteapi.connection;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import net.theskyboardpanel.remoteapi.connection.exceptions.ConnectionException;
import net.theskyboardpanel.remoteapi.packet.Packet;

/**
 * 
 * This class is used for connections
 * 
 * @author Roe
 *
 */
public abstract class Connection extends Thread
{
	
	private final Socket socket;
	private final ObjectOutputStream out;
	private final ObjectInputStream in;
	
	public Connection(Socket socket, ObjectOutputStream out, ObjectInputStream in)
	{
	
		super("Connection for " + socket.getInetAddress().getHostAddress());
		this.socket = socket;
		this.out = out;
		this.in = in;
		
	}
	
	/**
	 * 
	 * This method is used to get the connection socket<br />
	 * <b>ATTENTION!</b> it's very important that your field for the getSocket is named <b>EXACTLY</b> socket!
	 * 
	 * @return the connection socket
	 */
	public Socket getSocket()
	{
		
		return this.socket;
		
	}
	
	/**
	 * 
	 * This method is used to get the last packet recieved from recievePacket()
	 * 
	 * @return the last packet recieved
	 */
	public abstract Packet getPacket() throws ConnectionException;
	
	/**
	 * 
	 * This method will try to recieve a new packet from the connection. <br />
	 * <b>NOTE</b>: This will halt the connection to a packet is recieved!
	 * 
	 * @return the packet recieved from the connection
	 */
	public abstract Packet recievePacket() throws ConnectionException;
	
	/**
	 * 
	 * This method is used to send a new packet to the connection
	 * 
	 * @param packet the packet to send
	 */
	public abstract void sendPacket(Packet packet) throws ConnectionException;
	
	/**
	 * 
	 * This method is used to close the connection peacefully
	 * 
	 */
	public abstract void close() throws ConnectionException;

	public ObjectOutputStream getOut()
	{
	
		return this.out;
	
	}

	/**
	 * 
	 * This method is used to return the input stream of the connection
	 * 
	 * @return the input stream
	 */
	public ObjectInputStream getIn()
	{
	
		return this.in;
	}
	
}