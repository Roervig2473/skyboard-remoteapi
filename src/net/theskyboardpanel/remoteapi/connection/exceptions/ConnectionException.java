package net.theskyboardpanel.remoteapi.connection.exceptions;

import net.theskyboardpanel.remoteapi.connection.Connection;

/**
 * 
 * This exception is used for when an error occours in the connection
 * 
 * @author Roe
 *
 */
public class ConnectionException extends Exception
{

	private static final long serialVersionUID = -869817001778124059L;
	
	private Connection connection;
	
	public ConnectionException(Connection connection)
	{
		
		super("Internal connection error");
		
		this.setConnection(connection);
		
	}
	
	/**
	 * 
	 * This method is used to get the connection there triggered the exception
	 * 
	 * @return the troubled connection
	 */
	public Connection getConnection()
	{
		
		return this.connection;
		
	}
	
	private void setConnection(Connection connection)
	{
		
		this.connection = connection;
		
	}

}