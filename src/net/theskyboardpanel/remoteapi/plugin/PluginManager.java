package net.theskyboardpanel.remoteapi.plugin;

import java.io.File;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.command.CommandExecutor;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.connection.Connection;
import net.theskyboardpanel.remoteapi.event.Event;
import net.theskyboardpanel.remoteapi.event.Listener;
import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;
import net.theskyboardpanel.remoteapi.plugin.exceptions.InvalidDependenciesException;
import net.theskyboardpanel.remoteapi.plugin.exceptions.InvalidPluginDescriptionException;
import net.theskyboardpanel.remoteapi.plugin.exceptions.InvalidPluginException;

/**
 * 
 * This interface is used for the Plugin Manager
 * 
 * @author Roe
 *
 */
public interface PluginManager
{

	/**
	 * 
	 * This method is used to get a plugin on the server
	 * 
	 * @param pluginName the name of the plugin you want to get
	 * @return plugin the requested plugin if it exists, <b>NULL</b> if the plugin doesn't exist
	 */
	public Plugin getPlugin(String pluginName);
	
	/**
	 * 
	 * This method is used to get the list of plugins on the server
	 * 
	 * @return the list of plugins
	 */
	public Plugin[] getPlugins();
	
	/**
	 * 
	 * This method is used to set the command executor for a command
	 * 
	 * @param command the command to set the executor for
	 * @param executor the executor to handle the command
	 * @param plugin the plugin there wants to set the executor
	 */
	public void setCommandExecutor(Command command, CommandExecutor executor, Plugin plugin);
	
	/**
	 * 
	 * This method is used to get a command
	 * 
	 * @param command the command you want to get
	 * @return the requested command if it exists, will return <b>NULL</b> otherwise
	 */
	public Command getCommand(String command);
	
	/**
	 * 
	 * This method is used to load a plugin from a file
	 * 
	 * @param file the plugin file
	 * @return the enabled plugin
	 * @throws InvalidPluginException Thrown if the plugin is invalid
	 * @throws InvalidPluginDescriptionException Thrown if the plugin description file is invalid
	 * @throws InvalidDependenciesException Thrown if the plugin dependencies is invalid
	 */
	public Plugin loadPlugin(File file) throws InvalidPluginException, InvalidPluginDescriptionException, InvalidDependenciesException;

	/**
	 * 
	 * This method is  used to load the plugin description from the plugin file
	 * 
	 * @param file the plugin file
	 * @return the plugin description file
	 * @throws InvalidPluginDescriptionException Thrown if the plugin description is invalid
	 */
	public PluginDescription loadPluginDescription(File file) throws InvalidPluginDescriptionException;
	
	/**
	 * 
	 * This method is used to load the plugins from a directory
	 * 
	 * @param directory the plugin directory
	 * @return the array of plugins
	 */
	public Plugin[] loadPlugins(File directory);
	
	/**
	 * 
	 * This method is used to enable all the loaded plugins
	 * 
	 * @return the enabled plugins
	 */
	public Plugin[] enablePlugins();
	
	/**
	 * 
	 * This method is used to enable a plugin
	 * 
	 * @param plugin the plugin you want to enable
	 */
	public boolean enablePlugin(Plugin plugin);
	
	/**
	 * 
	 * This method is used to disable and clear all plugins.
	 * 
	 */
	public void clearPlugins();

	/**
	 * 
	 * This method is used to disable a plugin
	 * 
	 * @param plugin the plugin you want to disable
	 */
	public void disablePlugin(Plugin plugin);
	
	/**
	 * 
	 * This method is used to disable all the plugins
	 * 
	 */
	public void disablePlugins();
	
	/**
	 * 
	 * This method is used to register a listener
	 * 
	 * @param listener the listener you want to register
	 * @param plugin the plugin there wants to register the listener
	 */
	public void registerListener(Listener listener, Plugin plugin) throws IllegalAccessException;
	
	/**
	 * 
	 * This method is used to call an event
	 * 
	 * @param event the event to call
	 */
	public void callEvent(Event event);
	
	/**
	 * 
	 * This method is used to register a new connection type
	 * 
	 * @param connection the connection type you want to register
	 * @param plugin the plugin there wants to register the connection type
	 * @return true if the connection was successfully added, false if not
	 */
	public boolean registerConnectionType(Class<? extends Connection> connection, Plugin plugin);
	
	/**
	 * 
	 * This method is used to execute a command
	 * 
	 * @param sender the sender there's trying to execute the command
	 * @param commandString the command string that you want to execute
	 * @param args the command arguments
	 * @return true on success, false on error
	 * @throws InsufficientPermissionsException
	 */
	public boolean executeCommand(CommandSender sender, String commandString, String[] args) throws InsufficientPermissionsException;
	
}