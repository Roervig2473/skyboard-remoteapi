package net.theskyboardpanel.remoteapi.plugin;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.common.*;

/**
 * 
 * This interface is used to create the plugin information file
 * <b>NOTE:</b> Not all fields are optional!
 * 
 * @author Roe
 *
 */
public interface PluginDescription
{

	/**
	 * 
	 * This method is used to get the name of the plugin<br />
	 * <b>REQUIRED</b>
	 * 
	 * @return the name of the plugin
	 */
	public String getName();
	
	/**
	 * 
	 * This method is used to get the path to the main class of the plugin<br />
	 * <b>REQUIRED</b>
	 * 
	 * @return the path to the main class
	 */
	public String getMain();
	
	/**
	 * 
	 * This method is used to get the primary author of the plugin<br />
	 * <b>REQUIRED</b>
	 * 
	 * @return the primary author
	 */
	public Author getAuthor();
	
	/**
	 * 
	 * This method is used to get the addtional authors of the plugin
	 * <b>OPTIONAL</b>
	 * 
	 * @return the plugin authors
	 */
	public Author[] getAuthors();
	
	/**
	 * 
	 * This method is used to get the commands the plugin have registered<br />
	 * <b>OPTIONAL</b>
	 * 
	 * @return the commands the plugin have registered
	 */
	public Command[] getCommands();
	
	/**
	 * 
	 * This method is used to get the version of the plugin<br />
	 * <b>REQUIRED</b>
	 * 
	 * @return the plugin version
	 */
	public Version getVersion();
	
	/**
	 * 
	 * This method is used to get the list of plugins this plugin depends on
	 * 
	 * @return the plugin dependency list
	 */
	public String[] getDependencies();
	
}