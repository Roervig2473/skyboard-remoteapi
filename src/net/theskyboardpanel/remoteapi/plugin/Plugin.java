package net.theskyboardpanel.remoteapi.plugin;

import java.io.File;

import net.theskyboardpanel.remoteapi.command.*;
import net.theskyboardpanel.remoteapi.logging.Logger;

/**
 * 
 * This class is used to create plugins for the API
 * 
 * @author Roe
 *
 */
public abstract class Plugin implements CommandExecutor
{
	
	/**
	 * 
	 * This method is called when the plugin is loaded.<br />
	 * You cannot register commands, events, listeners and etc in this method.
	 * 
	 */
	public void onLoad()
	{
		
	}

	/**
	 * 
	 * This method is called when the plugin is getting enabled<br />
	 * 
	 * @return true if the plugin have been enabled successfully, false if not
	 * 
	 */
	public boolean onEnable()
	{
		
		return true;
		
	}
	
	/**
	 * 
	 * This method is called when the plugin is getting disabled
	 * 
	 */
	public void onDisable()
	{
		
	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandExecutor#onCommand
	 * 
	 */
	public boolean onCommand(CommandSender sender, Command command, String[] args)
	{
		
		return false;
		
	}
	
	/**
	 * 
	 * This method is used to check if the plugin is enabled or not
	 * 
	 * @return wether or not the plugin is enabled
	 */
	public abstract boolean isEnabled();
	
	/**
	 * 
	 * This method is used to get the assigned logger for the plugin
	 * 
	 * @return the plugin logger
	 */
	public abstract Logger getLogger();
	
	/**
	 * 
	 * This method is used to get the plugin description
	 * 
	 * @return the plugin description
	 */
	public abstract PluginDescription getPluginDescription();

	/**
	 * 
	 * This method is used to get the plugin manager
	 * 
	 * @return the plugin manager
	 */
	public abstract PluginManager getPluginManager();
	
	/**
	 * 
	 * This method used to get the folder of the plugin
	 * 
	 * @return the plugin folder
	 */
	public abstract File getLocation();
	
}