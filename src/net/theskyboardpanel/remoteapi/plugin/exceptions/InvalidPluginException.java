package net.theskyboardpanel.remoteapi.plugin.exceptions;

import net.theskyboardpanel.remoteapi.plugin.Plugin;

/**
 * 
 * This exception is thrown when a plugin is invalid
 * 
 * @author Roe
 *
 */
public class InvalidPluginException extends PluginException
{

	private static final long serialVersionUID = -7431346281864756016L;

	/**
	 * 
	 * The reason the plugin is invalid
	 *
	 * @param plugin the invalid plugin
	 * @param reason the reason when the plugin is invalid
	 */
	public InvalidPluginException(Plugin plugin, String reason)
	{
		
		super(plugin, "Invalid Plugin: " + reason);
		
	
	}

	/**
	 * 
	 * The reason the plugin is invalid
	 *
	 * @param reason the reason when the plugin is invalid
	 */
	public InvalidPluginException(String reason)
	{
		
		super("Invalid Plugin: " + reason);
		
	
	}
	
}