package net.theskyboardpanel.remoteapi.plugin.exceptions;

import net.theskyboardpanel.remoteapi.plugin.Plugin;

/**
 * 
 * This class is used when an error occoured in the plugin
 * 
 * @author Roe
 *
 */
public class PluginException extends RuntimeException
{

	private static final long serialVersionUID = 4391952638096195740L;
	private final Plugin plugin;
	
	/**
	 * 
	 * @param plugin the plugin there triggered the exception
	 * @param message the error message
	 */
	public PluginException(Plugin plugin, String message)
	{
		
		super(plugin.getPluginDescription().getName() + " v" + plugin.getPluginDescription().getVersion().toString() + " triggered an exception (" + message + ")");
		this.plugin = plugin;
		
	}
	
	/**
	 * 
	 * @param message the error message
	 */
	public PluginException(String message)
	{
		
		super("An unknown plugin triggered an exception (" + message + ")");
		this.plugin = null;
		
	}
	
	public Plugin getPlugin()
	{
		
		return this.plugin;
		
	}
	
}