package net.theskyboardpanel.remoteapi.plugin.exceptions;

import net.theskyboardpanel.remoteapi.plugin.Plugin;

/**
 * 
 * This exception is used when a plugin have some unmet/bad dependecies
 * 
 * @author Roe
 *
 */
public class InvalidDependenciesException extends InvalidPluginException
{

	private static final long serialVersionUID = 2549746237437594490L;

	/**
	 * 
	 * @param plugin the plugin with the invalid dependencies
	 * @param dependencies the invalid dependencies
	 */
	public InvalidDependenciesException(Plugin plugin, String dependencies)
	{
	
		super(plugin, "Invalid Dependenncies: " + dependencies);
		
	}

}