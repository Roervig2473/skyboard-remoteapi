package net.theskyboardpanel.remoteapi.plugin.exceptions;

import net.theskyboardpanel.remoteapi.plugin.Plugin;

public class IllegalAccessException extends PluginException
{

	private static final long serialVersionUID = 850619192718966529L;

	public IllegalAccessException(Plugin plugin, String message)
	{
	
		super(plugin, "Attempted to " + message + " but is not enabled");
		
	}

}