package net.theskyboardpanel.remoteapi.plugin.exceptions;

/**
 * 
 * This exception is used when a plugin contains an invalid description file
 * 
 * @author Roe
 *
 */
public class InvalidPluginDescriptionException extends InvalidPluginException
{

	private static final long serialVersionUID = -5761545794458293141L;

	/**
	 * 
	 * @param message the error message
	 */
	public InvalidPluginDescriptionException(String message)
	{
	
		super("Invalid Plugin Description: " + message);

	}

}