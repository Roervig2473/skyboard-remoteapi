package net.theskyboardpanel.remoteapi.services;

/**
 * 
 * This interface is 
 * 
 * @author Roe
 *
 */
public interface Service
{

	public void start(String[] commands);
	public void stop(String[] commands);
	public void install(String[] commands);
	public void suspend(String[] commands);
	public void terminate(String[] commands);
	public void executeCommand(String command);
	
}