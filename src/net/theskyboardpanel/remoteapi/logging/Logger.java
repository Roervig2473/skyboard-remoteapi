/* 
 * Copyright (C) 1997-2014 Nicolaj Christoffer Rørvig ALSO known as
 * JavaPortals, Roe, Roervig2473 AND ImThatPedoBear AND Chunky Hosting LLC
 * All Rights Reserved.
 *
 * This document is bound under the Chunky Hosting LLC software Terms 
 * of Use AND the individual terms of the script that this document belongs to and
 * MUST NOT be removed, distributed in any form, released
 * or modified, without written permission from the script authors.
 * 
 * The Chunky Hosting LLC software Terms of Use AND this software own
 * Terms of Use are agreed to upon installation of this softwares.
 * If you do not agree to them, then you're required to remove ALL Chunky Hosting LLC Softwares from
 * all systems you have installed it on.
 * 
 * http://theskyboardpanel.net/Terms/
 * 
 * The source code or encoded versions of the source code
 * myst ONLY exist on approved Chunky Hosting LLC Licensed Servers.
 * 
 * The SKYBoard Softwares is distributed in the hope that it will be usefull
 * but WITHOUT ANY WARRANTY; without even the implied warrantry of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package net.theskyboardpanel.remoteapi.logging;

import java.util.logging.Level;

/**
 * 
 * This interface is used for the loggers
 *
 * @author Roe
 *
 */
public interface Logger
{

	/**
	 * 
	 * This method is used to log a message with a predefined level
	 * 
	 * @param message the message to log
	 */
	public void log(String message);
	
	/**
	 * 
	 * This method is used to log a message with a custom log level
	 * 
	 * @param level the level to log on
	 * @param message the message to log
	 */
	public void log(Level level, String message);
	
	/**
	 * 
	 * This method is used to log an exception on a predefined level
	 * 
	 * @param e exception the exception to log
	 */
	public void log(Exception e);
	
	/**
	 * 
	 * This method is used to log an exception with a custom log level
	 * 
	 * @param level the level to log on
	 * @param e exception the exception to log
	 */
	public void log(Level level, Exception e);
	
	/**
	 * 
	 * This method is used to check if the logger is in debug mode
	 * 
	 * @return debug wether or not the logger is in debug mode
	 */
	public boolean isDebug();
	
	/**
	 * 
	 * This method is used to set the logger in debug mode
	 * 
	 * @param debug the debug mode
	 */
	public void setDebug(boolean debug);
	
}